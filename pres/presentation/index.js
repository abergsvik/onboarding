// Import React
import React from "react";

// Import Spectacle Core tags
import {
  Notes,
  // CodePane,
  Image,
  Deck,
  Heading,
  ListItem,
  List,
  Slide,
  Text
} from "spectacle";

import Icon from "./icon";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Require CSS
import "normalize.css";
import "./style.css";

const CodePane = function CodePane({ source }) {
  return (
    <pre
      style={{
        fontSize: "0.5em",
        textAlign: "left",
        maxHeight: "600px",
        overflow: "auto"
      }}
    >
      {source}
    </pre>);
};

const theme = createTheme({
  primary: "#9FD1CA",
  secondary: "#8E7FAE",
  tertiary: "#28334A",
  quarternary: "#CECECE",
  yellow: "#f8e266",
  text: "white"
}, {
  primary: { name: "Raleway", googleFont: true, styles: [ "400", "500", "800" ] },
  secondary: { name: "Raleway", googleFont: true, styles: [ "400", "500", "800" ] }
});

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck transition={["zoom", "slide"]} transitionDuration={500} theme={theme}>

        <Slide transition={["slide"]} bgColor="yellow">
          <Heading caps size={6} fit textColor="tertiary">Welcome</Heading>
        </Slide>

        <Slide transition={["zoom"]} bgColor="primary">
          <Text margin="10px 0 0" textColor="tertiary" size={1} fit bold>
            building a onboarding todo list with
          </Text>
          <Heading size={1} fit caps lineHeight={1} textColor="tertiary">
            <strong>React.JS</strong>
          </Heading>

        </Slide>

        <Slide transition={["fade"]} bgColor="primary">
          <Heading size={6} caps lineHeight={1} textColor="tertiary">
            What is React?
          </Heading>
          <Text textColor="tertiary" size={8}>A JavaScript library for building user interfaces</Text>
        </Slide>

        <Slide transition={["fade"]} bgColor="primary">
          <Notes>
            <h4>Vad ska vi göra</h4>
            <ol>
              <li>Vi tittar på reusable components</li>
              <li>update component state</li>
              <li>pass props</li>
              <li>Async / Await functions</li>
            </ol>
          </Notes>
          <Notes>
            <h4>Diet - other word for light</h4>
          </Notes>
          <Heading size={6} caps lineHeight={1} textColor="tertiary">
            What are we building?
          </Heading>
          <Text textColor="tertiary" size={8}>Diet Onboarding Todo List</Text>
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Heading size={6} textColor="white">Some ES6 Concepts used</Heading>
          <List textColor="white">
            <ListItem>Arrow functions</ListItem>
            <ListItem>Spread operator</ListItem>
            <ListItem>Async/Await promises</ListItem>
            <ListItem>Class/contructor</ListItem>
          </List>
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Heading size={6} textColor="white">Some ES6 Concepts used</Heading>
          <List textColor="white">
            <ListItem>{"const v = e => a = e"}</ListItem>
            <ListItem>{"{ ...obj }"}</ListItem>
            <ListItem>{"async function load() { return await fetch() }"}</ListItem>
            <ListItem>{"class Comp extends Component {}"}</ListItem>
          </List>
        </Slide>

        <Slide transition={["fade"]} bgColor="yellow">
          <Notes>
            <ul>
              <li>Öppna cmd+2</li>
              <li>localhost:3000</li>
            </ul>
          </Notes>
          <Heading size={6} fit caps textColor="tertiary">DEMO</Heading>
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Notes>
            <ul>
              <li>application entry point</li>
              <li>package.json</li>
            </ul>
          </Notes>
          <Heading size={6} textColor="white">{"index.js"}</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="jsx"
            source={`
import React from 'react';
import ReactDOM from 'react-dom';
import 'font-awesome/css/font-awesome.min.css'
import App from './App';
import './index.css';

ReactDOM.render(<App />, document.getElementById('root'));
`}
            margin="20px auto"
            overflow = "overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Notes>
            <ul>
              <li>innehåller alla våra komponenter</li>
            </ul>
          </Notes>
          <Heading size={6} textColor="white">{"<App />"}</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="jsx"
            source={`
            class App extends Component {
              render() {
                const { checklist, user, isFetching, toggleChecked } = this.props
                if (checklist && checklist.error) {
                  return <p>Ooooops! Something went wrong!</p>
                }
                if (isFetching) {
                  return <p>Loading...</p>
                }
                return (
                  <div className="App">
                    <h1>Hello {user.name}</h1>
                    {checklist && (
                      <Fragment>
                        <ProgressList list={checklist} />
                        <Checklist clickHandler={toggleChecked} list={checklist} />
                      </Fragment>
                    )}
                  </div>
                )
              }
            }

            App.propTypes = {
              ...withDataPropTypes,
            }

            export default withData(App);
            `}
            margin="20px auto"
            overflow = "overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Notes>
            <ul>
              <li>Inkludera checklist componenten</li>
            </ul>
          </Notes>
          <Heading size={6} textColor="white">Include checklist component</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="jsx"
            source={`
            <div className="App">
              <Checklist clickHandler={toggleChecked} list={checklist} />
            </div>
          `}
            margin="20px auto"
            overflow = "overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Notes>
            <h3>checklist</h3>
            <ul>
              <li>classnames</li>
              <li>map</li>
              <li>markdown</li>
            </ul>
          </Notes>
          <Heading size={6} textColor="white">{"<Checklist />"}</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="jsx"
            source={`
            function Checklist({ list, clickHandler }) {
              const _clickHandler = args => event => {
                clickHandler(args)
              }
              return (
                <ul className="list">
                  {list.map(li => {
                    const { id, completed, title, description } = li
                    const className = classnames({ li, 'li--completed': li.completed })
                    return (
                      <li className={className} key={id}>
                        <button
                          value={id}
                          className="btn li__button"
                          onClick={_clickHandler({id, completed: !completed })}
                          aria-label={\`Mark \${title} as completed\`}>
                          <Icon name={completed ? 'check-square' : 'square' }></Icon>
                          {title}
                        </button>
                        <VisibleToggle html className={'li__text'}>
                          {markdown.toHTML(description)}
                        </VisibleToggle>
                      </li>
                    )
                  })}
                </ul>
              )
            }
            `}
            margin="20px auto"
            overflow = "overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Notes>
            <ul>
              <li>Exempel på PureComponent</li>
              <li>dangerouslySetInnerHTML</li>
            </ul>
          </Notes>
          <Heading size={6} textColor="white">{"<VisibleToggle />"}</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="js"
            source={require("raw-loader!../assets/visible.example")}
            margin="20px auto"
            overflow="overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Notes>
            <ul>
              <li>Adding ternary "checklist && ()"</li>
              <li>Fragment</li>
              <li>Adding ProgressList</li>
            </ul>
          </Notes>
          <Heading size={6} textColor="white">{"App.jsx"}</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="jsx"
            source={`
            <div className="App">
              <h1>Hello {user.name}</h1>
              {checklist && (
                <Fragment>
                  <ProgressList list={checklist} />
                  <Checklist clickHandler={toggleChecked} list={checklist} />
                </Fragment>
              )}
            </div>
          `}
            margin="20px auto"
            overflow = "overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Notes>
            <ul>
              <li>list.filter</li>
              <li>chroma-js</li>
              <li>propTypes</li>
            </ul>
          </Notes>
          <Heading size={6} textColor="white">{"<ProgressList />"}</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="jsx"
            source={require("raw-loader!../assets/progress.example")}
            margin="20px auto"
            overflow = "overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Heading size={6} textColor="white">Where does the data come from?</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="jsx"
            source={`
            import withData, { withDataPropTypes } from './functions/WithData'

            ...

            const { checklist, user, isFetching, toggleChecked } = this.props

            if (checklist && checklist.error) {
              return <p>Ooooops! Something went wrong!</p>
            }
            if (isFetching) {
              return <p>Laddar...</p>
            }

            ...

            App.propTypes = {
              ...withDataPropTypes,
            }

            export default withData(App);
          `}
            margin="20px auto"
            overflow = "overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Heading size={6} textColor="white">Higher Order Component</Heading>
          <Text textColor="white">A function that wraps a component in a component...</Text>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="js"
            source={"export default withData(App)"}
            margin="20px auto"
            overflow="overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Notes>
            <ul>
              <li>Promises async/await</li>
              <li>import * as ... imports all exported things</li>
            </ul>
          </Notes>
          <Heading size={6} textColor="white">withData</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="js"
            source={require("raw-loader!../assets/withData.example")}
            margin="20px auto"
            overflow="overflow"
          />
        </Slide>

        <Slide transition={["fade"]} bgColor="tertiary">
          <Notes>
            <ul>
              <li>rest api with json-server</li>
            </ul>
          </Notes>
          <Heading size={6} textColor="white">{"api.js"}</Heading>
          <CodePane
            style={{ overflow: "scroll", height: "66vh" }}
            lang="js"
            source={require("raw-loader!../assets/api.example")}
            margin="20px auto"
            overflow = "overflow"
          />
        </Slide>

        <Slide transition={["zoom"]} bgColor="yellow">
          <Notes>
            <ul>
              <li>Kolla på error states - block url</li>
            </ul>
          </Notes>
          <Heading size={6} fit caps textColor="tertiary">DEMO</Heading>
        </Slide>

        <Slide transition={["slide"]} bgColor="primary">
          <Image src="http://media.giphy.com/media/cAEm5rSuuBEGY/giphy.gif" />
        </Slide>

        <Slide transition={["fade"]} bgColor="primary">
          <Heading size={6} fit textColor="tertiary">Thank you!</Heading>
          <List textColor="tertiary">
            <ListItem>bitbucket.org/abergsvik/onboarding</ListItem>
          </List>
        </Slide>

      </Deck>
    );
  }
}
