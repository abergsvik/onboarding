import React from "react";

const Icon = ({name}) => (
  <i className={`fa fa-${name}`} />
);

export default Icon;

