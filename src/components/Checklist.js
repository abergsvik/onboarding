import React from 'react'
import classnames from 'classnames'

import './checklist.css'
import { markdown } from 'markdown'
import VisibleToggle from './VisibleToggle'
import Icon from './Icon'

function Checklist({ list, clickHandler }) {
  const _clickHandler = args => event => {
    clickHandler(args)
  }
  return (
    <ul className="list">
      {list.map(li => {
        const { id, completed } = li
        const className = classnames({ li, 'li--completed': li.completed })
        return (
          <li className={className} key={li.id}>
            <button
              value={li.id}
              className="btn li__button"
              onClick={_clickHandler({id, completed: !completed })}
              aria-label={`Mark ${li.title} as completed`}>
              <Icon name={li.completed ? 'check-square' : 'square' }></Icon>
              {li.title}
            </button>
            <VisibleToggle html className={'li__text'}>
              {markdown.toHTML(li.description)}
            </VisibleToggle>
          </li>
        )
      })}
    </ul>
  )
}

export default Checklist
