import React, { Fragment, PureComponent } from 'react'
import classnames from 'classnames'

class VisibleToggle extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      visible: false
    }
    this.clickHandler = this.clickHandler.bind(this)
  }

  clickHandler() {
    this.setState((state) => {
      return {
        visible: !state.visible,
      }
    })
  }

  render() {
    const { children, className, html } = this.props
    const { visible } = this.state
    const buttonClass = classnames('fa', {
      'fa-caret-down': !visible,
      'fa-caret-up': visible,
    })
    return (
      <Fragment>
        <button
          className="btn center"
          aria-expanded={visible}
          aria-label="Read more"
          onClick={this.clickHandler}>
          <i className={buttonClass} />
        </button>
        {visible && (
          <div
            dangerouslySetInnerHTML={html ? {__html: children} : null}
            className={className}
            hidden={!visible}>
            {html ? null : children}
          </div>
        )}

      </Fragment>
    )
  }
}

export default VisibleToggle