import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'

function Icon ({name, spin, ...rest}) {
  const className = classnames('fa', `fa-${name}`, {'fa-spin': spin})
  return <i {...rest} className={className}></i>
}

Icon.defaultProps = {
  spin: false,
}

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  spin: PropTypes.bool
}

export default Icon