import React, { Fragment } from 'react'
import chroma from 'chroma-js'
import PropTypes from 'prop-types'
import './progress.css'

function Progress ({ list }) {
  if(!list) {
    return null
  }

  const completedItems = list.filter(item => item.completed === true)
  const number = completedItems.length / list.length

  const progress = Math.floor(number * 100)

  const scale = chroma.scale(['#c16d5f', '#F9E267', '#9fd1ca'])
  const color = scale(number).hex()

  const innerStyle = {
    width: `${progress}%`,
    backgroundColor: color,
  }

  return (
    <Fragment>
      <h3 className="progress__label">Completed {completedItems.length}/{list.length} tasks.</h3>
      <div className="progress">
        <span className="progress__bar" style={innerStyle}/>
      </div>
    </Fragment>
  )
}

Progress.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape({
    completed: PropTypes.bool,
  })),
}

export default Progress