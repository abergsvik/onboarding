import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as api from '../api';

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component'
}


function withData(WrappedComponent) {
  class WithData extends Component {
    constructor(props) {
      super(props)
      this.toggleChecked = this.toggleChecked.bind(this)
      this.state = {
        user: {
          name: '',
        },
        checklist: [],
        isFetching: true,
      }
    }

    componentDidMount() {
      this.loadContent()
    }

    loadContent() {
      Promise.all([this.loadChecklist(), this.getUser()])
        .then(values => {
          this.setState({
            checklist: values[0].checklist,
            user: values[1].user,
            isFetching: false,
          })
        })
    }

    async getUser() {
      try {
        const user = await api.getUser()
        return { user }
      } catch (error) {
        return { user: {error} }
      }
    }

    async loadChecklist() {
      try {
        const checklist = await api.getChecklist()
        return { checklist }
      } catch (error) {
        return { checklist: {error} }
      }
    }

    updateChecklistItem ({checklist, id, completed}) {
      const index = checklist.findIndex(i => i.id === id)
      const updatedChecklist = checklist.map((item, i) => {
        if(i === index) {
          return {
            ...item,
            completed,
          }
        }
        return { ...item }
      })
      return updatedChecklist
    }

    async toggleChecked({ id, completed }) {
      const { checklist } = this.state
      const updatedChecklist = this.updateChecklistItem({
        checklist, id, completed
      })

      // Set state before network is completed
      this.setState({
        checklist: updatedChecklist,
      })

      try {
        await api.toggleChecked({ id, completed })
      } catch (error) {
        // Reset the state if we failed
        this.setState({
          checklist
        })
        // Log the error
        console.warn(error)
      }
    }

    render() {
      return (
        <WrappedComponent
          toggleChecked={this.toggleChecked}
          {...this.state}
        />
      )
    }
  }
  WithData.displayName = `WithData(${getDisplayName(WrappedComponent)})`
  return WithData
}

export const withDataPropTypes = {
  checklist: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  user: PropTypes.object.isRequired,
  isFetching: PropTypes.bool.isRequired,
  toggleChecked: PropTypes.func.isRequired,
}

export default withData