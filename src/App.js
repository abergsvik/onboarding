import React, { Component, Fragment } from 'react';
import Checklist from './components/Checklist'
import ProgressList from './components/ProgressList'
import withData, { withDataPropTypes } from './functions/WithData'

class App extends Component {
  render() {
    const { checklist, user, isFetching, toggleChecked } = this.props

    if (checklist && checklist.error) {
      return <p>Ooooops! Something went wrong!</p>
    }

    if (isFetching) {
      return <p>Laddar...</p>
    }

    return (
      <Fragment>
        <header className="header">Boarding ON</header>
        <div className="App">
          <div className="col">
            <h1>Hello {user.name}</h1>
            <div className="text">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ullamcorper rutrum pretium. In non lectus volutpat, cursus sapien sit amet, pretium turpis. Sed tincidunt sodales maximus. Duis malesuada dolor sapien, ut accumsan erat blandit tempus. Nulla porta eleifend nibh a interdum. Donec facilisis leo vitae gravida mollis. Aliquam commodo augue ac gravida egestas.</p>
              <p>Morbi et sem libero. In hendrerit placerat justo, et dignissim diam sodales et. Sed vitae pretium felis, a fermentum diam. Duis vulputate lacinia diam, a tristique mauris. Etiam placerat faucibus felis, non condimentum quam facilisis ac. Nullam egestas bibendum diam. Nullam consequat erat quis ante cursus, quis dapibus urna pulvinar. Etiam id lacus iaculis, feugiat orci eu, accumsan mi. Mauris non ultrices erat, ac varius ipsum. Mauris interdum leo dolor, quis luctus nisl imperdiet vel. Proin suscipit ligula vehicula suscipit eleifend. Sed luctus arcu in sem hendrerit suscipit.</p>
            </div>
          </div>
          <div className="col">
            <ProgressList list={checklist} />
            <Checklist clickHandler={toggleChecked} list={checklist} />
          </div>
        </div>
      </Fragment>
    )
  }
}

App.propTypes = {
  ...withDataPropTypes,
}

export default withData(App);
